import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  domain = `http://dummy.restapiexample.com`;
  constructor(
    public http: HttpClient
  ) { }


  /**
   * @method :  Create a new user with user Data
   * @request  :  user modal
   */
  createUser(req): Observable<any> {
    return this.http.post(`${this.domain}/api/v1/create`, req)
      .pipe(
        map(res => res),
        catchError(err => {
          return throwError(err)
        })
      )
  }


  /**
  * @method :  update existing user by user Id and user data
  * @param  :  user Id
  * @request  :  user modal
  */
  updateUser(req): Observable<any> {
    return this.http.put(`${this.domain}/api/v1/update/${req && req.id}`, req)
      .pipe(
        map(res => res),
        catchError(err => {
          return throwError(err)
        })
      )
  }



  /**
  * @method :  Get existing user by user Id 
  * @param  :  user Id
  * @request  :  null
  */
  getSignleUser(id): Observable<any> {
    console.log(id);
    return this.http.get(`${this.domain}/api/v1/employee/${id}`)
      .pipe(
        map(res => res),
        catchError(err => {
          return throwError(err)
        })
      )
  }

  /**
  * @method :  Get existing user list 
  * @param  :  null
  * @request  :  null
  */
  getUserList(): Observable<any> {
    console.log(`${this.domain}/api/v1/employees`);
    return this.http.get(`${this.domain}/api/v1/employees`)
      .pipe(
        map(res => res),
        catchError(err => {
          return throwError(err)
        })
      )
  }

  /**
  * @method :  Delete existing user by user Id 
  * @param  :  user Id
  * @request  :  null
  */
  deleteUserById(id): Observable<any> {
    return this.http.delete(`${this.domain}/api/v1/delete/${id}`)
      .pipe(
        map(res => res),
        catchError(err => {
          return throwError(err)
        })
      )
  }


}
